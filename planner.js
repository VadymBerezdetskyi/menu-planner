import eachDayOfInterval from 'date-fns/eachDayOfInterval';
import isSaturday from 'date-fns/isSaturday';
import isSunday from 'date-fns/isSunday';

const 

/**
 *
 * @param {*} mealsList
 * @param {*} chosenList
 * @param {*} day
 * @param {*} count
 * @returns {string[]}
 */
const chooseMeals = (mealsList, chosenList, day, count) =>
  mealsList
    .slice()
    .sort((a, b) => rateMeal(a, day, chosenList) - rateMeal(b, day, chosenList))
    .slice(0, count)
    .map(m => m.id);

export const createPlanner = db => {
  const { meals, ingredients, positions, warehouse } = db;

  return {
    /**
     * @param {Date} from
     * @param {Date} to
     * @param {number} mealsPerDay
     * @returns {{date: Date, meals: string[]}[]}
     */
    plan: (from, to, mealsPerDay) =>
      eachDayOfInterval({ start: from, end: to }).reduce(
        (chosen, date) => [
          ...acc,
          {
            date,
            meals: chooseMeals(meals, chosen, date, mealsPerDay),
          },
        ],
        []
      ),
    getShoppingList: () => {
      console.log('Shopping list');
    },
  };
};
