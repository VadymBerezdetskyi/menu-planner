/**
 * @param {{meal: string, date: Date}[]} plan
 * @returns {object}
 */
const formatPlan = plan => {};

/**
 * @param {{position: string, quantity: number}[]} plan
 * @returns {object}
 */
const formatShoppingList = shoppingList => {};

export const createApp = (db, plannerFactory) => {
  const planner = plannerFactory(db);

  return {
    getPlan: plan => {
      formatPlan(plan);
    },
    getShoppingList: shoppingList => {
      formatShoppingList(shoppingList);
    },
  };
};
