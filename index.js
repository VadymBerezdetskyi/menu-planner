import fs from 'fs';
import { createPlanner } from './planner';

const db = JSON.parse(fs.readFileSync('./db/db.json'));
const planner = createPlanner(db);

const args = process.argv.slice(2);

const menu = planner.plan(args[0] || new Date(), args[1]);
console.log(plan);

const shoppingList = planner.getShoppingList(menu.map(p => p.meal));
console.log(shoppingList);
